package com.bto.git.training.controller;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bto.git.training.domain.User;
import com.bto.git.training.repo.UserRepository;

@RestController
@RequestMapping(value = "/api/user", produces = "application/json")
public class UserController {
	private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public User add(final @RequestBody  @Valid User userDto) {
      User userEntity = new User();
      BeanUtils.copyProperties(userDto, userEntity);
      return userRepository.save(userEntity);
    }
}
