package com.bto.git.training.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bto.git.training.domain.User;
@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	
}
